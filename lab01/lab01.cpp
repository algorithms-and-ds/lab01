﻿#include <stdio.h>
#include <time.h>
#include <windows.h>

void saveCurrentTimeAndDate() {
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    time_t currentTime;
    struct tm* localTime;

    currentTime = time(NULL);
    localTime = localtime(&currentTime);

    printf("1. Поточний час: %02d:%02d:%02d\n", localTime->tm_hour, localTime->tm_min, localTime->tm_sec);
    printf("   Поточна дата: %02d.%02d.%04d\n", localTime->tm_mday, localTime->tm_mon + 1, localTime->tm_year + 1900);
}

void inputSignedShort() {
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    signed short input;
    printf("2. Введіть ціле число типу signed short: ");
    scanf("%hd", &input);

    if (input >= 0) {
        printf("   Значення: %hd (позитивне)\n", input);
    }
    else {
        printf("   Значення: %hd (від'ємне)\n", input);
    }
}

void performOperations() {
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    signed char a = 5;
    signed char b = 127;
    int userInput;

    printf("3. Введіть результат операції 'а' (5 + 127): ");
    while (1) {
        if (scanf("%d", &userInput) == 1 && userInput == a + b) {
            printf("   Правильно! Результат в двійковій системі 10000100\n");
            break;
        }
        else {
            printf("   Помилка! Введіть правильне значення: ");
            while (getchar() != '\n'); 
        }
    }

    printf("Введіть результат операції 'б' (2 - 3): ");
    while (1) {
        if (scanf("%d", &userInput) == 1 && userInput == 2 - 3) {
            printf("   Правильно! Результат в двійковій системі 11111111\n");
            break;
        }
        else {
            printf("   Помилка! Введіть правильне значення: ");
            while (getchar() != '\n'); 
        }
    }

    printf("Введіть результат операції 'в' (-120 - 34): ");
    while (1) {
        if (scanf("%d", &userInput) == 1 && userInput == -120 - 34) {
            printf("   Правильно! Результат в двійковій системі 11111111111111111111111101101010\n");
            break;
        }
        else {
            printf("   Помилка! Введіть правильне значення: ");
            while (getchar() != '\n');
        }
    }

    printf("Введіть результат операції 'г' ((unsigned char)(-5)): ");
    while (1) {
        if (scanf("%d", &userInput) == 1 && userInput == (unsigned char)(-5)) {
            printf("   Правильно! Результат в двійковій системі 11111011\n");
            break;
        }
        else {
            printf("   Помилка! Введіть правильне значення: ");
            while (getchar() != '\n'); 
        }
    }

    printf("Введіть результат операції 'д' (56 & 38): ");
    while (1) {
        if (scanf("%d", &userInput) == 1 && userInput == (56 & 38)) {
            printf("   Правильно! Результат в двійковій системі 00100000\n");
            break;
        }
        else {
            printf("   Помилка! Введіть правильне значення: ");
            while (getchar() != '\n');
        }
    }

    printf("Введіть результат операції 'е' (56 | 38): ");
    while (1) {
        if (scanf("%d", &userInput) == 1 && userInput == (56 | 38)) {
            printf("   Правильно! Результат в двійковій системі 00111110\n");
            break;
        }
        else {
            printf("   Помилка! Введіть правильне значення: ");
            while (getchar() != '\n'); 
        }
    }
}

union FloatUnion {
    float floatValue;
    struct {
        unsigned int mantissa : 23;
        unsigned int exponent : 8;
        unsigned int sign : 1;
    } parts;
};
struct FloatParts {
    unsigned int sign;
    unsigned int exponent;
    unsigned int mantissa;
};

void saveFloatValue() {
    float myFloat;
    printf("4. Введіть дійсне число типу float: ");
    scanf("%f", &myFloat);
    struct FloatParts parts;
    unsigned int* floatBits = (unsigned int*)&myFloat;
    parts.sign = (*floatBits >> 31) & 1;
    parts.exponent = (*floatBits >> 23) & 0xFF;
    parts.mantissa = *floatBits & 0x7FFFFF;
    int userInput;

    printf("Введіть значення побітово: ");
    while (1) {
        if (scanf("%x", &userInput) == 1 && userInput == *floatBits) {
            printf("   Правильно!\n");
            break;
        }
        else {
            printf("   Помилка! Введіть правильне значення: ");
            while (getchar() != '\n'); 
        }
    }

    unsigned char byte1, byte2, byte3, byte4;
    printf("Введіть значення побайтово (через пропуск): ");
    while (1) {
        if (scanf("%hhx %hhx %hhx %hhx", &byte1, &byte2, &byte3, &byte4) == 4 &&
            byte1 == ((unsigned char*)floatBits)[0] &&
            byte2 == ((unsigned char*)floatBits)[1] &&
            byte3 == ((unsigned char*)floatBits)[2] &&
            byte4 == ((unsigned char*)floatBits)[3]) {
            printf("   Правильно!\n");
            break;
        }
        else {
            printf("   Помилка! Введіть правильне значення: ");
            while (getchar() != '\n');
        }
    }

    printf("Введіть значення знаку: ");
    while (1) {
        if (scanf("%d", &userInput) == 1 && userInput == parts.sign) {
            printf("   Правильно!\n");
            break;
        }
        else {
            printf("   Помилка! Введіть правильне значення: ");
            while (getchar() != '\n'); 
        }
    }

    printf("Введіть значення мантиси: ");
    while (1) {
        if (scanf("%d", &userInput) == 1 && userInput == parts.mantissa) {
            printf("   Правильно!\n");
            break;
        }
        else {
            printf("   Помилка! Введіть правильне значення: ");
            while (getchar() != '\n');
        }
    }

    printf("Введіть значення ступеня: ");
    while (1) {
        if (scanf("%d", &userInput) == 1 && userInput == parts.exponent - 127) {
            printf("   Правильно!\n");
            break;
        }
        else {
            printf("   Помилка! Введіть правильне значення: ");
            while (getchar() != '\n'); 
        }
    }
}

int main() {
    saveCurrentTimeAndDate();
    inputSignedShort();
    performOperations();
    saveFloatValue();

    return 0;
}
